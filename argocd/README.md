# ArgoCD

## Install ArgoCD and set admin password

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
kubectl -n argocd patch secret argocd-secret -p '{"stringData": { "admin.password": "$2a$12$qYExnZPEIvrpMfc6CSvCXO9Pua9N6OvbikjwFADqPj.V9yjfOmQ6q", "admin.passwordMtime": "'$(date +%FT%T%Z)'"}}'
```

## Add application
```bash
kubectl apply -f vapormap.yaml 
```
